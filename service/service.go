package service

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"go.uber.org/zap"
	"prasadhebbar.com/template/repo"
)

// UserService provides user operations.
type UserService interface {
	CreateUser(ctx context.Context, email string, password string) (string, error)
	GetUser(ctx context.Context, id string) (string, error)
	Search(ctx context.Context, search string) (string, error)
}

type UserServiceImpl struct {
	Repository repo.Repository
	Logger     *zap.Logger
}

func NewService(rep repo.Repository, logger *zap.Logger) UserService {
	return &UserServiceImpl{
		Repository: rep,
		Logger:     logger,
	}
}

func (s UserServiceImpl) CreateUser(ctx context.Context, email string, password string) (string, error) {
	id := "123456" //Dummy
	user := repo.User{
		ID:       id,
		Email:    email,
		Password: password,
	}

	if err := s.Repository.CreateUser(ctx, user); err != nil {
		s.Logger.Error("CreateUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("CreateUser", zap.String("New user id", id))

	return "Success", nil
}

func (s UserServiceImpl) GetUser(ctx context.Context, id string) (string, error) {

	email, err := s.Repository.GetUser(ctx, id)

	if err != nil {
		s.Logger.Error("GetUser", zap.String("error", err.Error()))
		return "", err
	}

	s.Logger.Info("CreateUser", zap.String("New user id", id))

	return email, nil
}

//Example of calling another service
func (s UserServiceImpl) Search(ctx context.Context, query string) (string, error) {
	jsonData := fmt.Sprintf(`{"id": "%s"}`, query)

	req, err := http.NewRequest("POST", "http://localhost:8080/get_user", bytes.NewBuffer([]byte(jsonData)))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	var results string
	newCtx, cancel := context.WithTimeout(ctx, time.Second*4) //Set appropriate timeout
	defer cancel()
	err = httpDo(newCtx, req, func(resp *http.Response, err error) error {
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		data, readErr := ioutil.ReadAll(resp.Body)
		if readErr != nil {
			s.Logger.Error("Error reading service response", zap.String("error", err.Error()))
			return readErr
		}

		results = string(data)
		return nil
	})

	return results, err
}

func httpDo(ctx context.Context, req *http.Request, f func(*http.Response, error) error) error {
	// Run the HTTP request in a goroutine and pass the response to f.
	c := make(chan error, 1)
	req = req.WithContext(ctx)
	go func() {
		c <- f(http.DefaultClient.Do(req))
	}()
	select {
	case <-ctx.Done():
		return ctx.Err() //Not waiting for function to return in case of Context error
	case err := <-c:
		return err
	}
}
