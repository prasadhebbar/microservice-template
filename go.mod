module prasadhebbar.com/template

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-kit/kit v0.12.0
	github.com/go-swagger/go-swagger v0.28.0 // indirect
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.3 // indirect
	github.com/pdrum/swagger-automation v0.0.0-20190629163613-c8c7c80ba858
	github.com/stretchr/testify v1.7.0 // indirect
	go.uber.org/zap v1.19.1
)
