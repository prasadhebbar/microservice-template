package main

import (
	"context"
	"log"
	"testing"

	"go.uber.org/zap"
	"prasadhebbar.com/template/repo"
	"prasadhebbar.com/template/service"
)

func TestServiceCreateUser(t *testing.T) {
	var testService service.UserServiceImpl

	db, _ := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	testService = service.UserServiceImpl{repo, logger}

	t.Log("Creating user with random email id and password.")
	got, err := testService.CreateUser(context.Background(), "test@email.com", "test")
	want := "Success"
	if got != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
	if err != nil {
		t.Error("Error occured" + err.Error())
	}

}

func TestServiceGetUser(t *testing.T) {
	var testService service.UserServiceImpl
	db, _ := NewMock()
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal("Unable to initiate logging... aborting")
	}
	defer logger.Sync()
	repo := &repo.Repo{db, logger}
	testService = service.UserServiceImpl{repo, logger}

	t.Log("Testing with id '12345'")
	got, err := testService.GetUser(context.Background(), "12345")
	want := "prasad.hebbar@gmail.com"
	if got != want {
		t.Errorf("got %s, wanted %s", got, want)
	}
	if err != nil {
		t.Error("Error occured" + err.Error())
	}

}

func BenchmarkGetUser(b *testing.B) {
	var testService service.UserServiceImpl

	for i := 0; i < b.N; i++ {
		testService.GetUser(context.Background(), "12345")
	}
}
