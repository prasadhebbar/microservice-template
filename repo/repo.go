package repo

import (
	"context"
	"database/sql"
	"errors"

	"go.uber.org/zap"
)

type User struct {
	ID       string `json:"id,omitempty"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Repository interface {
	CreateUser(ctx context.Context, user User) error
	GetUser(ctx context.Context, id string) (string, error)
}

type Repo struct {
	DB     *sql.DB
	Logger *zap.Logger
}

func NewRepo(db *sql.DB, logger *zap.Logger) Repository {
	return &Repo{
		DB:     db,
		Logger: logger,
	}
}

//Todo Have SQLs as prepared statements
func (repo *Repo) CreateUser(ctx context.Context, user User) error {
	sql := "INSERT INTO users (id, email, password) VALUES ($1, $2, $3)"

	if user.Email == "" || user.Password == "" {
		return errors.New("Blank email or password")
	}

	_, err := repo.DB.ExecContext(ctx, sql, user.ID, user.Email, user.Password)
	if err != nil {
		return err
	}
	return nil
}

func (repo *Repo) GetUser(ctx context.Context, id string) (string, error) {
	var email string
	err := repo.DB.QueryRowContext(ctx, "SELECT email FROM users WHERE id=$1", id).Scan(&email)
	if err != nil {
		repo.Logger.Error("repo.GetUser", zap.String("error", err.Error()))
		return "", err
	}

	return email, nil
}
