package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
	_ "prasadhebbar.com/template/doc"
	"prasadhebbar.com/template/middleware"
	"prasadhebbar.com/template/repo"
	"prasadhebbar.com/template/service"
	"prasadhebbar.com/template/transport"
)

/*

1. Add stub for calling other microservice in goroutine

*/

func main() {
	var logger *zap.Logger
	var err error
	{
		logger, err = zap.NewProduction()
		if err != nil {
			log.Fatal("Unable to initiate logging... aborting")
		}
		defer logger.Sync()
	}

	logger.Info("Service started")
	defer logger.Info("Service ended")

	httpAddr := os.Getenv("HTTP_ADDR") //host:port
	if len(httpAddr) == 0 {
		httpAddr = ":8080"
	}
	dbName := os.Getenv("DB_NAME")
	if len(dbName) == 0 {
		logger.Fatal("No database name provided")
	}

	dbUserName := os.Getenv("DB_USERNAME")
	if len(dbUserName) == 0 {
		logger.Fatal("No database user name provided")
	}

	dbPassword := os.Getenv("DB_PASSWORD")
	if len(dbPassword) == 0 {
		logger.Fatal("No database password provided")
	}

	dbAddr := os.Getenv("DB_ADDR") //host:port
	if len(dbAddr) == 0 {
		dbAddr = "localhost:5432"
	}

	dbFlags := os.Getenv("DB_FLAGS") //flag1=val1&flag2=val2 etc

	dbSource := fmt.Sprintf("postgresql://%s:%s@%s/%s?%s", dbUserName, dbPassword, dbAddr, dbName, dbFlags)

	var db *sql.DB
	{
		var err error

		db, err = sql.Open("postgres", dbSource)
		if err != nil {
			logger.Error("Unable to open database", zap.String("error", err.Error()))
			os.Exit(-1)
		}

	}
	defer db.Close()

	flag.Parse()

	var srv service.UserService
	{
		repository := repo.NewRepo(db, logger)

		srv = service.NewService(repository, logger)
	}
	srv = middleware.LoggingMiddleware{logger, srv}

	errs := make(chan error)

	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	endpoints := transport.MakeEndpoints(srv)

	go func() {
		logger.Info("Server started", zap.String("port", httpAddr))
		handler := transport.NewHTTPServer(context.Background(), endpoints)
		errs <- http.ListenAndServe(httpAddr, handler)
	}()

	logger.Error("Error starting server", zap.String("error", (<-errs).Error()))
}
